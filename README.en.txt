**********************************************************************

  MIDITrail source code Ver.1.0.1 for iOS

  Copyright (C) 2010-2014 WADA Masashi. All Rights Reserved.

  Web : http://sourceforge.jp/projects/miditrail/
  Mail: yknk@users.sourceforge.jp

**********************************************************************

(1) Introduction

  This is the entire source code of MIDITrail for iOS.

(2) Development environment

  OS X 10.8.5 (Mountain Lion)
  Xcode 4.6.3

(3) Folders

  Sources/MIDITrail
    The application project.
    It implements the processing of rendering by OpenGL ES.
    It uses "OGLUtility", "SMIDILib" and "YNBaseLib".

  Source/OGLUtility
    The OpenGL ES Utility project.
    It implements the wrapping for OpenGL ES APIs.
    It uses "YNBaseLib".

  Sources/SMIDILib
    The Simple MIDI Library project.
    It implements the processing of MIDI control and analyzing note informantion.
    It uses "YNBaseLib".

  Sources/YNBaseLib
    The Basic Library Project.
    It implements the error control and utility functions.

(4) License

  MIDITrail is released under the BSD license.
  Please check "LICENSE.txt".


