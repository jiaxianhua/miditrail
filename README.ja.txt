**********************************************************************

  MIDITrail ソースコード Ver.1.0.1 for iOS

  Copyright (C) 2010-2014 WADA Masashi. All Rights Reserved.

  Web : http://sourceforge.jp/projects/miditrail/
  Mail: yknk@users.sourceforge.jp

**********************************************************************

(1) 概要

  MIDITrail for iOS の全ソースコードです。

(2) ビルド環境

  OS X 10.8.5 (Mountain Lion)
  Xcode 4.6.3

(3) フォルダ構成

  Sources/MIDITrail
    アプリケーション本体のプロジェクトです。
    OpenGL ESを用いた描画処理を実装しています。
    OGLUtility, SMIDILib, YNBaseLib を利用しています。

  Source/OGLUtility
    OpenGL ESユーティリティのプロジェクトです。
    OpenGL ES APIのラッパークラスを実装しています。
    YNBaseLib を利用しています。

  Sources/SMIDILib
    シンプルMIDIライブラリのプロジェクトです。
    MIDIデータの再生とノート情報参照に特化したライブラリです。
    YNBaseLib を利用しています。

  Sources/YNBaseLib
    基本ライブラリのプロジェクトです。
    エラー制御やユーティリティ関数を含んでいます。

(4) ライセンス

  修正BSDライセンスを適用して公開しています。 
  詳細は LICENSE.txt を参照してください。


