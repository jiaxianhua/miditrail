//******************************************************************************
//
// MIDITrail / main.mm
//
// エントリポイント
//
// Copyright (C) 2012 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import <UIKit/UIKit.h>
#import "MIDITrailAppDelegate.h"


//******************************************************************************
// メイン関数
//******************************************************************************
int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([MIDITrailAppDelegate class]));
	}
}

