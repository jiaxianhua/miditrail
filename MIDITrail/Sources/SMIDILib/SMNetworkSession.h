//******************************************************************************
//
// Simple MIDI Library / SMNetworkSession
//
// ネットワークセッションクラス
//
// Copyright (C) 2012 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import <CoreMIDI/MIDINetworkSession.h>


//******************************************************************************
// ネットワークセッションクラス
//******************************************************************************
class SMNetworkSession
{
public:	
	
	//コンストラクタ／デストラクタ
	SMNetworkSession();
	virtual ~SMNetworkSession();
	
	//初期化
	int Initialize();
	
	//有効化
	void Enable();
	
	//無効化
	void Disable();
	
private:
	
	MIDINetworkSession* m_pNetworkSession;
	
};

