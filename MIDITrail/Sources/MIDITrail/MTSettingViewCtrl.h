//******************************************************************************
//
// MIDITrail / MTSettingViewCtrl
//
// 設定画面制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import <UIKit/UIKit.h>
#import "YNBaseLib.h"
#import "SMIDILib.h"
#import "MTSettingEncodingViewCtrl.h"


//******************************************************************************
// モニタービュー制御クラス
//******************************************************************************
@interface MTSettingViewCtrl : UIViewController {
	
	//テーブルビュー
	IBOutlet UITableView* m_pTableView;
	
	//ユーザ設定
	YNUserConf* m_pUserConf;
	
	//MIDIデバイス制御
	SMOutDevCtrl m_OutDevCtrl;
	SMInDevCtrl m_InDevCtrl;
	
	//ビューモードアイコン画像
	UIImage* m_pImagePianoRoll3D;
	UIImage* m_pImagePianoRoll2D;
	UIImage* m_pImagePianoRollRain;
	UIImage* m_pImagePianoRollRain2D;
	
	//エンコーディング設定ビュー
	MTSettingEncodingViewCtrl* m_pSettingEncodingViewCtrl;
	
}

//生成
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

//ビュー登録完了
- (void)viewDidLoad;

//ビュー解除完了
- (void)viewDidUnload;

//インターフェース自動回転確認
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

//インターフェース自動回転確認（iOS6以降）
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;

//ビュー表示
- (void)viewWillAppear:(BOOL)animated;

//ビュー非表示
- (void)viewWillDisappear:(BOOL)animated;

//セクション数
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView;

//セクションヘッダ
- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section;

//セクションごとの項目数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

//項目表示内容
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント
- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath;

//選択エンコーディングID取得
- (NSStringEncoding)selectedEncodingId;


@end
