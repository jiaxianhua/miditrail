//******************************************************************************
//
// MIDITrail / MTHowToViewCtrl
//
// HowToビュー制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "YNBaseLib.h"
#import "MTHowToViewCtrl.h"


//******************************************************************************
// プライベートメソッド定義
//******************************************************************************
@interface MTHowToViewCtrl ()

@end


@implementation MTHowToViewCtrl

//******************************************************************************
// 生成
//******************************************************************************
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//******************************************************************************
// ビュー登録完了
//******************************************************************************
- (void)viewDidLoad
{	
	NSString* pFileName = nil;
	NSString* pHtmlPath = nil;
	
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	//HTMLファイル名
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		//iPhone / iPod touch
		pFileName = @"doc/HowToView-iPhone.html";
	}
	else {
		//iPad
		pFileName = @"doc/HowToView-iPad.html";
	}
	
	//HTMLファイルパス生成
	pHtmlPath = [NSString stringWithFormat:@"%@/%@", [YNPathUtil resourceDirPath], pFileName];
	
	//HTMLファイル表示
	m_pWebView.dataDetectorTypes = UIDataDetectorTypeNone;
	m_pWebView.scalesPageToFit = YES;
	[m_pWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:pHtmlPath]]];
	
	return;
}

//******************************************************************************
// ビュー解除完了
//******************************************************************************
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
	
	return;
}

//******************************************************************************
// インターフェース自動回転確認
//******************************************************************************
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	BOOL isRotate = NO;
	
	if ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
		|| (interfaceOrientation == UIInterfaceOrientationLandscapeRight)) {
		isRotate = YES;
	}
	
	return isRotate;
}

//******************************************************************************
// インターフェース自動回転確認（iOS6以降）
//******************************************************************************
- (BOOL)shouldAutorotate
{
	//回転を許可する
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認：回転方向（iOS6以降）
//******************************************************************************
- (NSUInteger)supportedInterfaceOrientations
{
	//横長のみに対応する
	return UIInterfaceOrientationMaskLandscape;
}

//******************************************************************************
// ビュー表示
//******************************************************************************
- (void)viewWillAppear:(BOOL)animated
{
	return;
}

//******************************************************************************
// ビュー非表示
//******************************************************************************
- (void)viewWillDisappear:(BOOL)animated
{
	return;
}

//******************************************************************************
// 完了ボタン押下
//******************************************************************************
- (IBAction)onDoneButton
{
	[self dismissModalViewControllerAnimated:YES];
}


@end

