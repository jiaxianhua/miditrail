//******************************************************************************
//
// MIDITrail / MIDITrailApp
//
// MIDITrail アプリケーションクラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import <Foundation/Foundation.h>
#import "YNBaseLib.h"
#import "SMIDILib.h"
#import "OGLUtil.h"
#import "MTParam.h"
#import "MTScene.h"
#import "MTTabBarCtrl.h"
#import "MTFileViewCtrl.h"
#import "MTMonitorViewCtrl.h"
#import "MTSettingViewCtrl.h"
#import "MTHelpViewCtrl.h"
#import "MTMainViewCtrl.h"


//******************************************************************************
// MIDITrail アプリケーションクラス
//******************************************************************************
@interface MIDITrailApp : NSObject {
	
	//ウィンドウ
	UIWindow* m_pWindow;
	
	//タブバー制御
	MTTabBarCtrl* m_pTabBarCtrl;

	//ナビゲーション制御
	UINavigationController* m_pFileNaviCtrl;
	UINavigationController* m_pMonitorNaviCtrl;
	UINavigationController* m_pSettingNaviCtrl;
	UINavigationController* m_pHelpNaviCtrl;
	
	//ビュー制御
	MTFileViewCtrl* m_pFileViewCtrl;
	MTMonitorViewCtrl* m_pMonitorViewCtrl;
	MTSettingViewCtrl* m_pSettingViewCtrl;
	MTHelpViewCtrl* m_pHelpViewCtrl;
	MTMainViewCtrl* m_pMainViewCtrl;
	
	//レンダリング系
	MTScene* m_pScene;
	OGLRedererParam m_RendererParam;
	
	//MIDI制御系
	SMSeqData m_SeqData;
	SMSequencer m_Sequencer;
	SMMsgQueue m_MsgQueue;
	SMLiveMonitor m_LiveMonitor;
	SMNetworkSession m_NetworkSession;
	
	//演奏状態
	PlayStatus m_PlayStatus;
	bool m_isRepeat;
	bool m_isRewind;
	unsigned long m_PlaySpeedRatio;
	
	//表示効果
	bool m_isEnablePianoKeyboard;
	bool m_isEnableRipple;
	bool m_isEnablePitchBend;
	bool m_isEnableStars;
	bool m_isEnableCounter;
	
	//シーン種別
	SceneType m_SceneType;
	SceneType m_SelectedSceneType;
	
	//設定ファイル
	YNUserConf* m_pUserConf;
	
	//アプリケーションアクティブ状態
	bool m_isAppActive;
	
	//リワインド／スキップ制御
	int m_SkipBackTimeSpanInMsec;
	int m_SkipForwardTimeSpanInMsec;
	
	//演奏スピード制御
	int m_SpeedStepInPercent;
	int m_MaxSpeedInPercent;
	
}

//初期化
- (id)init;

//初期化
- (int)initialize:(NSDictionary*)pLaunchOptions;

//実行
- (int)run;

//停止
- (int)terminate;

//ファイル選択イベント
- (void)onSelectFile:(NSNotification*)pNotification;

//シーン生成完了通知
- (void)onFinishCreateScene:(NSNotification*)pNotification;

//クローズボタン押下
- (void)onCloseButton:(NSNotification*)pNotification;

//再生ボタン押下
- (void)onPlayButton:(NSNotification*)pNotification;

//停止ボタン押下
- (void)onStopButton:(NSNotification*)pNotification;

//後方スキップボタン押下
- (void)onSkipBackwardButton:(NSNotification*)pNotification;

//前方スキップボタン押下
- (void)onSkipForwardButton:(NSNotification*)pNotification;

//再生スピードダウンボタン押下
- (void)onPlaySpeedDownButton:(NSNotification*)pNotification;

//再生スピードアップボタン押下
- (void)onPlaySpeedUpButton:(NSNotification*)pNotification;

//リピートボタン押下
- (void)onRepeatButton:(NSNotification*)pNotification;

//演奏状態変更通知：一時停止
- (void)onChangePlayStatusPause:(NSNotification*)pNotification;

//演奏状態変更通知：停止（演奏終了）
- (void)onChangePlayStatusStop:(NSNotification*)pNotification;

//モニタ開始イベント
- (void)onStartMonitoring:(NSNotification*)pNotification;

//非アクティブ状態遷移
- (void)onApplicationWillResignActive;

//バックグラウンド状態遷移
- (void)onApplicationDidEnterBackground;

@end

