//******************************************************************************
//
// MIDITrail / MTTouchCtrl
//
// タッチイベント制御クラス
//
// Copyright (C) 2012 WADA Masashi. All Rights Reserved.
//
//******************************************************************************


//******************************************************************************
// タッチイベント制御クラス
//******************************************************************************
class MTTouchCtrl
{
public:
	
	//操作種別
	enum OperationType {
		OperationNone,		//なし
		OperationEyeDir,	//視線方向
		OperationMoveFB,	//移動：前後
		OperationMoveLR,	//移動：左右
		OperationMoveUD,	//移動：上下
		OperationRotate		//回転
	};
	
public:
	
	//コンストラクタ／デストラクタ
	MTTouchCtrl();
	virtual ~MTTouchCtrl();
	
	//初期化
	void Initialize(float width, float height);
	
	//タッチイベント
	void OnTouchBegan(float x, float y);
	void OnTouchMoved(float x, float y);
	void OnTouchEnd(float x, float y);
	void OnTouchCanceled(float x, float y);
	
	//デルタ値取得
	float GetDeltaEyeDirX();
	float GetDeltaEyeDirY();
	float GetDeltaFB();
	float GetDeltaLR();
	float GetDeltaUD();
	float GetDeltaRT();
	
	//デルタ値クリア
	void ClearDelta();
	
	//操作種別取得
	OperationType GetCurOperationType();
	
private:
	
	//スクリーンサイズ
	float m_ScreenWidth;
	float m_ScreenHeight;
	
	//タッチ状態
	bool m_isTouching;
	
	//前回タッチ位置
	float m_PrevX;
	float m_PrevY;
	
	//今回タッチ位置
	float m_CurX;
	float m_CurY;
	
	//操作種別
	OperationType m_OperationType;
	
	//エッジ幅
	float m_EdgeWidth;
	
	void _Clear();
	OperationType _GetOperationTypeOfArea(float x, float y);

};

