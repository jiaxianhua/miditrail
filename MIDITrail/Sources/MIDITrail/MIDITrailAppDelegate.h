//******************************************************************************
//
// MIDITrail / MIDITrailAppDelegate
//
// MIDITrail アプリケーションデリゲート
//
// Copyright (C) 2012 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import <UIKit/UIKit.h>
#import "MIDITrailApp.h"


//******************************************************************************
// アプリケーションデリゲートクラス
//******************************************************************************
@interface MIDITrailAppDelegate : UIResponder <UIApplicationDelegate> {
	
	//アプリケーションオブジェクト
	MIDITrailApp* m_pApp;
	
}

//アプリケーション起動処理終了
- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions;

//非アクティブ状態遷移（電話着信など）
- (void)applicationWillResignActive:(UIApplication*)application;

// バックグラウンド状態遷移
- (void)applicationDidEnterBackground:(UIApplication*)application;

//フォアグラウンド状態遷移：バックグラウンド状態→非アクティブ状態
- (void)applicationWillEnterForeground:(UIApplication*)application;

//アクティブ状態遷移
- (void)applicationDidBecomeActive:(UIApplication*)application;

//アプリケーション終了
- (void)applicationWillTerminate:(UIApplication*)application;

//破棄
- (void)dealloc;


@end

