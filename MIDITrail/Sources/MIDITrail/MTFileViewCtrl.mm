//******************************************************************************
//
// MIDITrail / MTFileViewCtrl
//
// ファイルビュー制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "MTFileViewCtrl.h"


//******************************************************************************
// プライベートメソッド定義
//******************************************************************************
@interface MTFileViewCtrl ()

//ファイル一覧生成
- (int)makeFileList;

//通知送信処理
- (int)postNotificationWithName:(NSString*)pName;

//テーブルセル作成：ファイル
- (UITableViewCell*)makeFileCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント：ファイル一覧
- (void)onSelectFileCellForIndexPath:(NSIndexPath*)indexPath;

@end


@implementation MTFileViewCtrl

//******************************************************************************
// 生成
//******************************************************************************
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		//ビュー設定
		self.title = @"MIDI Files";
		self.tabBarItem.image = [UIImage imageNamed:@"img/TabIcon"];
		
		//ファイル一覧生成
		[self makeFileList];
		
		//選択行番号
		m_SelectedFileIndex = 0;
    }
    return self;
}

//******************************************************************************
// ビュー登録完了
//******************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Do any additional setup after loading the view.
	
	return;
}

//******************************************************************************
// ビュー解除完了
//******************************************************************************
- (void)viewDidUnload
{
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
	
	return;
}

//******************************************************************************
// インターフェース自動回転確認
//******************************************************************************
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認（iOS6以降）
//******************************************************************************
- (BOOL)shouldAutorotate
{
	//回転を許可する
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認：回転方向（iOS6以降）
//******************************************************************************
- (NSUInteger)supportedInterfaceOrientations
{
	//全方向に対応する
	return UIInterfaceOrientationMaskAll;
}

//******************************************************************************
// ビュー表示
//******************************************************************************
- (void)viewWillAppear:(BOOL)animated
{
	return;
}

//******************************************************************************
// ビュー非表示
//******************************************************************************
- (void)viewWillDisappear:(BOOL)animated
{
	return;
}

//******************************************************************************
// ファイル一覧生成
//******************************************************************************
- (int)makeFileList
{
	int result = 0;
	int i = 0;
	NSArray* pPathList = nil;
	NSString* pDocDirPath = nil;
	NSString* pPath = nil;
	NSString* pFile = nil;
	NSArray* pFileArrayTmp = nil;
	NSMutableArray* pFileArray = nil;
	BOOL isDir = NO;
	
	pFileArray = [[NSMutableArray alloc] init];
	
	//Documentsディレクトリパスを取得
	pPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	pDocDirPath = [pPathList objectAtIndex:0];
	
	//ディレクトリ配下のファイル一覧を取得
	pFileArrayTmp = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:pDocDirPath error:nil];
	
	//ファイル一覧を作成（ディレクトリは除外する）
	for (i = 0; i < [pFileArrayTmp count]; i++) {
		pFile = [pFileArrayTmp objectAtIndex:i];
		pPath = [pDocDirPath stringByAppendingPathComponent:pFile];
		[[NSFileManager defaultManager] fileExistsAtPath:pPath isDirectory:&isDir];
		if (!isDir) {
			[pFileArray addObject:pFile];
		}
	}
	
	//ファイル名ソート：大文字小文字を区別しない
	m_pFileArray = [pFileArray sortedArrayUsingComparator:^(id str1, id str2) {
		return [((NSString*)str1) compare:((NSString*)str2) options:NSCaseInsensitiveSearch]; }];
	
	//テスト：ファイルが存在しない場合
	//m_pFileArray = [[NSArray alloc] init];
	
	if ([m_pFileArray count] == 0) {
		m_pTableView.allowsSelection = NO;
	}
	
	return result;
}

//******************************************************************************
// 通知送信処理
//******************************************************************************
- (int)postNotificationWithName:(NSString*)pName
{
	int result = 0;
	NSNotification* pNotification = nil;
	NSNotificationCenter* pCenter = nil;
	
	//通知オブジェクトを作成
    pNotification = [NSNotification notificationWithName:pName
												  object:self
												userInfo:nil];
	//通知する
	pCenter = [NSNotificationCenter defaultCenter];
	
	//通知に対応する処理を演奏スレッドで処理させる場合
	//[pCenter postNotification:pNotification];
	
	//通知に対応する処理をメインスレッドに処理させる場合
	[pCenter performSelectorOnMainThread:@selector(postNotification:)
							  withObject:pNotification
						   waitUntilDone:NO];
	
	return result;
}

//******************************************************************************
// セクション数
//******************************************************************************
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
	//セクション
	// 0. ファイル一覧
    return 1;
} 

//******************************************************************************
// セクションヘッダ
//******************************************************************************
- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString* pSectionHeader = nil;
	
	switch(section) {
		case 0:
			//ファイル一覧
			pSectionHeader = @"";
			break;
		default:
			break;
    }
	
    return pSectionHeader;
} 

//******************************************************************************
// セクションごとの項目数
//******************************************************************************
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
	NSInteger numOfRows = 0;
	
	//NSLog(@"section %d", section);
	
	switch (section) {
		case 0:
			//ファイル一覧
			numOfRows = [m_pFileArray count];
			
			//ファイルなしの場合はメッセージを表示
			if (numOfRows == 0) {
				numOfRows = 1;
			}
			break;
		default:
			break;
	}
	
	return numOfRows;
}

//******************************************************************************
// 項目表示内容
//******************************************************************************
- (UITableViewCell*)tableView:(UITableView*)tableView
		cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	UITableViewCell* pCell = nil;
	
	switch (indexPath.section) {
		case 0:
			//ファイル一覧
			pCell = [self makeFileCellForIndexPath:indexPath];
			break;
		default:
			break;
	}
	
    return pCell;
}

//******************************************************************************
// テーブルセル作成：ファイル
//******************************************************************************
- (UITableViewCell*)makeFileCellForIndexPath:(NSIndexPath*)indexPath
{
    static NSString* pCellIdentifier = @"MTFileViewCtrl";
	UITableViewCell* pCell = nil;
	
	//再利用可能セル生成
	pCell = [m_pTableView dequeueReusableCellWithIdentifier:pCellIdentifier];
	if (pCell == nil) {
		pCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
									   reuseIdentifier:pCellIdentifier];
	}
	
	//ラベル設定
	if ([m_pFileArray count] == 0) {
		//ファイルなしの場合はメッセージを表示
		pCell.textLabel.text = @"File not found.";
		pCell.detailTextLabel.text = @"You can import your MIDI files through iTunes File Sharing.";
		pCell.detailTextLabel.numberOfLines = 0;
	}
	else {
		//ファイル名を表示
		pCell.textLabel.text = [m_pFileArray objectAtIndex:indexPath.row];
	}
	
	return pCell;
}

//******************************************************************************
// テーブルセル作成：列の高さ
//******************************************************************************
- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
	CGFloat height = 0.0f;
	
	if ([m_pFileArray count] == 0) {
		//ファイルなしの場合はメッセージ表示のため高さを変更する
		height = 80.0f;
	}
	else {
		//デフォルトの高さを返す
		height = m_pTableView.rowHeight;
	}
	
	return height;
}

//******************************************************************************
// テーブルセル選択イベント
//******************************************************************************
- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
	//選択状態解除
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	switch (indexPath.section) {
		case 0:
			//ファイル一覧
			[self onSelectFileCellForIndexPath:indexPath];
			break;
		default:
			break;
	}
	
	return;
}

//******************************************************************************
// テーブルセル選択イベント：ファイル一覧
//******************************************************************************
- (void)onSelectFileCellForIndexPath:(NSIndexPath*)indexPath
{
	//NSLog(@"selected %d", indexPath.row);
	
	//ファイルなしの場合は何もしない
	if ([m_pFileArray count] == 0) goto EXIT;
	
	//選択行番号を保持
	m_SelectedFileIndex = indexPath.row;
	
	//ファイル選択通知を発行
	[self postNotificationWithName:@"onSelectFile"];
	
EXIT:;
	return;
}

//******************************************************************************
// 選択行ファイルパス取得
//******************************************************************************
- (NSString*)selectedFilePath
{
	NSArray* pPathList = nil;
	NSString* pDocDirPath = nil;
	NSString* pFileName = nil;
	NSString* pFilePath = nil;
	
	//Documentsディレクトリパスを取得
	pPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	pDocDirPath = [pPathList objectAtIndex:0];
	
	//選択行のファイル名
	pFileName = [m_pFileArray objectAtIndex:m_SelectedFileIndex];
	
	//ファイルパス
	pFilePath = [NSString stringWithFormat:@"%@/%@", pDocDirPath, pFileName];
	
	return pFilePath;
}

@end

