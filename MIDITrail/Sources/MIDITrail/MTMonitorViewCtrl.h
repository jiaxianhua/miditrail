//******************************************************************************
//
// MIDITrail / MTMonitorViewCtrl
//
// モニタビュー制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import <UIKit/UIKit.h>
#import "YNBaseLib.h"


//******************************************************************************
// モニタービュー制御クラス
//******************************************************************************
@interface MTMonitorViewCtrl : UIViewController {
	
	//デバイス名ラベル
	IBOutlet UILabel* m_pDeviceLabel;
	
	//ユーザ設定
	YNUserConf* m_pUserConf;
	
}

//生成
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

//ビュー登録完了
- (void)viewDidLoad;

//ビュー解除完了
- (void)viewDidUnload;

//インターフェース自動回転確認
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

//インターフェース自動回転確認（iOS6以降）
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;

//ビュー表示
- (void)viewWillAppear:(BOOL)animated;

//ビュー非表示
- (void)viewWillDisappear:(BOOL)animated;

//モニタリング開始ボタン押下
- (IBAction)onStartMonitoringButton;

@end

