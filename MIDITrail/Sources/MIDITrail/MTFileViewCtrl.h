//******************************************************************************
//
// MIDITrail / MTFileViewCtrl
//
// ファイルビュー制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import <UIKit/UIKit.h>


//******************************************************************************
// ファイルビュー制御クラス
//******************************************************************************
@interface MTFileViewCtrl : UIViewController {
	
	//テーブルビュー
	IBOutlet UITableView* m_pTableView;
	
	//ファイル名一覧
	NSArray* m_pFileArray;
	
	//選択ファイルインデックス
	NSInteger m_SelectedFileIndex;
	
}

//生成
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

//ビュー登録完了
- (void)viewDidLoad;

//ビュー解除完了
- (void)viewDidUnload;

//インターフェース自動回転確認
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

//インターフェース自動回転確認（iOS6以降）
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;

//ビュー表示
- (void)viewWillAppear:(BOOL)animated;

//ビュー非表示
- (void)viewWillDisappear:(BOOL)animated;

//セクション数
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView;

//セクションヘッダ
- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section;

//セクションごとの項目数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

//項目表示内容
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath;

//テーブルセル作成：列の高さ
- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント
- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath;

//選択行ファイルパス取得
- (NSString*)selectedFilePath;


@end

