//******************************************************************************
//
// MIDITrail / MTMainViewCtrl
//
// メインビュー制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import "SMIDILib.h"
#import "OGLUtil.h"
#import "MTParam.h"
#import "MTMachTime.h"
#import "MTScene.h"
#import "MTSceneMsgQueue.h"
#import "MTHowToViewCtrl.h"


//******************************************************************************
// メインビュー制御クラス
//******************************************************************************
@interface MTMainViewCtrl : UIViewController <UIActionSheetDelegate> {
	
	//ベースビュー
	IBOutlet UIView* m_pBaseView;
	
	//ボタン
	IBOutlet UIButton* m_pCloseButton;
	IBOutlet UIButton* m_pPlayButton;
	IBOutlet UIButton* m_pStopButton;
	IBOutlet UIButton* m_pSkipBackwardButton;
	IBOutlet UIButton* m_pSkipForwardButton;
	IBOutlet UIButton* m_pPlaySpeedDownButton;
	IBOutlet UIButton* m_pPlaySpeedUpButton;
	IBOutlet UIButton* m_pRepeatButton;
	IBOutlet UIButton* m_pViewButton;
	IBOutlet UIButton* m_pHelpButton;
	IBOutlet UIActivityIndicatorView* m_pActivityIndicator;
	IBOutlet UILabel* m_pLabel;
	
	//ガイドイメージ
	IBOutlet UIImageView* m_pGuideImageUD;
	IBOutlet UIImageView* m_pGuideImageFB;
	IBOutlet UIImageView* m_pGuideImageLR;
	IBOutlet UIImageView* m_pGuideImageRT;
	
	//スクリーンサイズ
	GLint m_ScreenWidth;
	GLint m_ScreenHeight;
	
	//OpenGL制御系
	EAGLContext* m_pEAGLContext;
	GLuint m_FrameBuffer;
	GLuint m_ColorRenderBuffer;
	GLuint m_DepthRenderBuffer;
	id m_Timer;
	
	//描画制御系
	OGLRenderer m_Renderer;
	OGLRedererParam m_RendererParam;
	MTScene* m_pScene;
	MTMachTime m_MachTime;
	
	//メッセージ制御系
	SMMsgQueue* m_pMsgQueue;
	MTSceneMsgQueue m_SceneMsgQueue;
	BOOL m_isStopScene;
	
	//画面制御系
	uint64_t m_LastTouchTime;
	uint64_t m_PrevDrawTime;
	float m_FPS;
	
	//タッチ制御
	BOOL m_isTap;
	
	//ボタン表示状態
	BOOL m_isDisplayButtons;
	
	//モニタフラグ
	BOOL m_isMonitor;
	
	//ヘルプビュー
	MTHowToViewCtrl* m_pHowToViewCtrl;
	
	UIImage* m_pImgButtonPlayN;
	UIImage* m_pImgButtonPlayH;
	UIImage* m_pImgButtonPauseN;
	UIImage* m_pImgButtonPauseH;
	
}

//--------------------------------------
// 生成と破棄
//--------------------------------------
//生成
- (id)initWithNibName:(NSString *)nibNameOrNil
			   bundle:(NSBundle *)nibBundleOrNil
		rendererParam:(OGLRedererParam)rendererParam;

//破棄
- (void)dealloc;

//初期化処理
- (int)initialize:(SMMsgQueue*)pMsgQueue;

//終了処理
- (void)terminate;

//描画デバイス取得
- (OGLDevice*)getDevice;

//--------------------------------------
// ビューイベント
//--------------------------------------
//ビュー取得
- (UIView*)getView;

//ビュー登録完了
- (void)viewDidLoad;

//ビュー解除完了
- (void)viewDidUnload;

//インターフェース自動回転確認
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

//インターフェース自動回転確認（iOS6以降）
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;

// ビュー表示開始
- (void)viewWillAppear:(BOOL)animated;

// ビュー表示終了
- (void)viewDidDisappear:(BOOL)animated;

//--------------------------------------
// シーン操作
//--------------------------------------
//シーン開始
- (int)startScene:(MTScene*)pScene isMonitor:(BOOL)isMonitor;

//シーン停止
- (int)stopScene;

//シーン操作：演奏開始
- (int)scene_PlayStart;

//シーン操作：演奏終了
- (int)scene_PlayEnd;

//シーン操作：巻き戻し
- (int)scene_Rewind;

//シーン操作：視点リセット
- (int)scene_ResetViewpoint;

//シーン操作：視点登録
- (int)scene_SetViewpoint:(MTScene::MTViewParamMap*)pParamMap;

//シーン操作：視点取得
- (int)scene_GetViewpoint:(MTScene::MTViewParamMap*)pParamMap;

//シーン操作：エフェクト設定
- (int)scene_SetEffect:(MTScene::EffectType)type isEnable:(bool)isEnable;

//--------------------------------------
// ボタンイベント
//--------------------------------------
// クローズボタン押下
- (IBAction)onCloseButton;
// 再生ボタン押下
- (IBAction)onPlayButton;
// 停止ボタン押下
- (IBAction)onStopButton;
// 後方スキップボタン押下
- (IBAction)onSkipBackwardButton;
// 前方スキップボタン押下
- (IBAction)onSkipForwardButton;
// 再生スピードダウンボタン押下
- (IBAction)onPlaySpeedDownButton;
// 再生スピードアップボタン押下
- (IBAction)onPlaySpeedUpButton;
// リピートボタン押下
- (IBAction)onRepeatButton;
// ビューボタン押下
- (IBAction)onViewButton;
// ヘルプボタン押下
- (IBAction)onHelpButton;

//アクションシートボタン選択イベント
- (void)actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;

//--------------------------------------
// タッチイベント
//--------------------------------------
//タッチイベント：開始
- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent *)event;

//タッチイベント：移動
- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent *)event;

//タッチイベント：終了
- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent *)event;

//タッチイベント：キャンセル
- (void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent *)event;

//--------------------------------------
// 状態設定
//--------------------------------------
//リピート状態設定
- (void)setRepeatStatus:(BOOL)isRepeat;

//インジケータアニメーション開始
- (void)startActivityIndicator;

//インジケータアニメーション停止
- (void)stopActivityIndicator;

//カレントコンテキスト設定
- (void)setCurrentContext;

//演奏状態設定
- (void)setPlayStatus:(PlayStatus)status;


@end

