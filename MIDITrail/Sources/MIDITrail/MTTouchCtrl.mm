//******************************************************************************
//
// MIDITrail / MTTouchCtrl
//
// タッチイベント制御クラス
//
// Copyright (C) 2012 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "MTTouchCtrl.h"


//******************************************************************************
// パラメータ定義
//******************************************************************************
//画面端の幅
#define EDGE_WIDTH_IPHONE	(40)
#define EDGE_WIDTH_IPAD		(80)

//******************************************************************************
// コンストラクタ
//******************************************************************************
MTTouchCtrl::MTTouchCtrl()
{
	_Clear();
	
	m_ScreenWidth = 0.0f;
	m_ScreenHeight = 0.0f;
	m_EdgeWidth = 0.0f;
	
	return;
}

//******************************************************************************
// デストラクタ
//******************************************************************************
MTTouchCtrl::~MTTouchCtrl()
{
	return;
}

//******************************************************************************
// クリア
//******************************************************************************
void MTTouchCtrl::_Clear()
{
	m_isTouching = false;
	m_PrevX = 0.0f;
	m_PrevY = 0.0f;
	m_CurX = 0.0f;
	m_CurY = 0.0f;
	m_OperationType = OperationNone;
}

//******************************************************************************
// 初期化
//******************************************************************************
void MTTouchCtrl::Initialize(float width, float height)
{
	_Clear();
	
	m_ScreenWidth = width;
	m_ScreenHeight = height;
	
	//デバイス種別
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		//iPhone / iPod touch
		m_EdgeWidth = EDGE_WIDTH_IPHONE;
	} else {
		//iPad
		m_EdgeWidth = EDGE_WIDTH_IPAD;
    }
	
	return;
}

//******************************************************************************
// タッチイベント：開始
//******************************************************************************
void MTTouchCtrl::OnTouchBegan(float x, float y)
{
	m_isTouching = true;
	m_PrevX = x;
	m_PrevY = y;
	m_CurX = x;
	m_CurY = y;
	m_OperationType = _GetOperationTypeOfArea(x, y);
	
	return;
}

//******************************************************************************
// タッチイベント：移動
//******************************************************************************
void MTTouchCtrl::OnTouchMoved(float x, float y)
{
	//NSLog(@"x:%f y:%f", x, y);
	
	m_CurX = x;
	m_CurY = y;
		
	return;
}

//******************************************************************************
// タッチイベント：終了
//******************************************************************************
void MTTouchCtrl::OnTouchEnd(float x, float y)
{
	_Clear();
	
	return;
}

//******************************************************************************
// タッチイベント：キャンセル
//******************************************************************************
void MTTouchCtrl::OnTouchCanceled(float x, float y)
{
	_Clear();
	
	return;
}

//******************************************************************************
// デルタ値取得：視線方向 X軸
//******************************************************************************
float MTTouchCtrl::GetDeltaEyeDirX()
{
	float delta = 0.0f;
	
	if (m_OperationType == OperationEyeDir) {
		delta = m_CurX - m_PrevX;
	}
	
	return delta;
}

//******************************************************************************
// デルタ値取得：視線方向 Y軸
//******************************************************************************
float MTTouchCtrl::GetDeltaEyeDirY()
{
	float delta = 0.0f;
	
	if (m_OperationType == OperationEyeDir) {
		delta = m_CurY - m_PrevY;
	}
	
	return delta;
}

//******************************************************************************
// デルタ値取得：前後方向
//******************************************************************************
float MTTouchCtrl::GetDeltaFB()
{
	float delta = 0.0f;
	
	if (m_OperationType == OperationMoveFB) {
		if ((_GetOperationTypeOfArea(m_PrevX, m_PrevY) == OperationMoveFB)
		 && (_GetOperationTypeOfArea(m_CurX, m_CurY) == OperationMoveFB)) {
			delta = m_CurY - m_PrevY;
		}
	}
	
	return delta;
}

//******************************************************************************
// デルタ値取得：左右方向
//******************************************************************************
float MTTouchCtrl::GetDeltaLR()
{
	float delta = 0.0f;
	
	if (m_OperationType == OperationMoveLR) {
		if ((_GetOperationTypeOfArea(m_PrevX, m_PrevY) == OperationMoveLR)
		 && (_GetOperationTypeOfArea(m_CurX, m_CurY) == OperationMoveLR)) {
			delta = m_CurX - m_PrevX;
		}
	}
	
	return delta;
}

//******************************************************************************
// デルタ値取得：上下方向
//******************************************************************************
float MTTouchCtrl::GetDeltaUD()
{
	float delta = 0.0f;
	
	if (m_OperationType == OperationMoveUD) {
		if ((_GetOperationTypeOfArea(m_PrevX, m_PrevY) == OperationMoveUD)
		 && (_GetOperationTypeOfArea(m_CurX, m_CurY) == OperationMoveUD)) {
			delta = m_CurY - m_PrevY;
		}
	}
	
	return delta;	
}

//******************************************************************************
// デルタ値取得：回転
//******************************************************************************
float MTTouchCtrl::GetDeltaRT()
{
	float delta = 0.0f;
	
	if (m_OperationType == OperationRotate) {
		if ((_GetOperationTypeOfArea(m_PrevX, m_PrevY) == OperationRotate)
			&& (_GetOperationTypeOfArea(m_CurX, m_CurY) == OperationRotate)) {
			delta = m_CurX - m_PrevX;
		}
	}
	
	return delta;	
}

//******************************************************************************
// デルタ値クリア
//******************************************************************************
void MTTouchCtrl::ClearDelta()
{
	m_PrevX = m_CurX;
	m_PrevY = m_CurY;
}

//******************************************************************************
// エリア操作種別取得
//******************************************************************************
MTTouchCtrl::OperationType MTTouchCtrl::_GetOperationTypeOfArea(float x, float y)
{
	OperationType type = OperationNone;
	
	//  +--+--------------+--+
	//  |  |      4       |  |
	//  |  +--------------+  |
	//  |  |              |  |
	//  |1 |      5       |2 |
	//  |  |              |  |
	//  |  +--------------+  |
	//  |  |      3       |  |
	//  +--+--------------+--+
	// 
	//  1 : Up / Down
	//  2 : Forward / Backward
	//  3 : Left / Right
	//  4 : Rotate
	//  5 : Eye Direction 
	
	type = OperationEyeDir;
	
	if (x < m_EdgeWidth) {
		type = OperationMoveUD;
	}
	else if ((m_ScreenWidth - m_EdgeWidth) < x) {
		type = OperationMoveFB;
	}
	else if ((m_ScreenHeight - m_EdgeWidth) < y) {
		type = OperationMoveLR;
	}
	else if (y < m_EdgeWidth) {
		type = OperationRotate;
	}
	
	return type;
}

//******************************************************************************
// 操作種別取得
//******************************************************************************
MTTouchCtrl::OperationType MTTouchCtrl::GetCurOperationType()
{
	return m_OperationType;
}



