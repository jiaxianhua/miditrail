//******************************************************************************
//
// MIDITrail / MTMonitorViewCtrl
//
// モニタビュー制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "SMIDILib.h"
#import "MTParam.h"
#import "MTMonitorViewCtrl.h"


//******************************************************************************
// プライベートメソッド定義
//******************************************************************************
@interface MTMonitorViewCtrl ()

//通知送信処理
- (int)postNotificationWithName:(NSString*)pName;

@end


@implementation MTMonitorViewCtrl

//******************************************************************************
// 生成
//******************************************************************************
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		//ビュー設定
		self.title = @"MIDI IN Monitor";
		self.tabBarItem.image = [UIImage imageNamed:@"img/TabIcon"];
    }
    return self;
}

//******************************************************************************
// ビュー登録完了
//******************************************************************************
- (void)viewDidLoad
{
	int result = 0;
	
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	//ユーザ設定初期化
	m_pUserConf = [[YNUserConf alloc] init];
	if (m_pUserConf == nil) {
		result = YN_SET_ERR(@"Program error.", 0, 0);
		goto EXIT;
	}
	
EXIT:;
	if (result != 0) YN_SHOW_ERR();
}

//******************************************************************************
// ビュー解除完了
//******************************************************************************
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
	
	m_pUserConf = nil;
	
	return;
}

//******************************************************************************
// インターフェース自動回転確認
//******************************************************************************
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認（iOS6以降）
//******************************************************************************
- (BOOL)shouldAutorotate
{
	//回転を許可する
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認：回転方向（iOS6以降）
//******************************************************************************
- (NSUInteger)supportedInterfaceOrientations
{
	//横長のみに対応する
	return UIInterfaceOrientationMaskLandscape;
}

//******************************************************************************
// ビュー表示
//******************************************************************************
- (void)viewWillAppear:(BOOL)animated
{
	int result = 0;
	unsigned long i = 0;
	NSString* pSelectedDevIdName = nil;
	NSString* pDevIdName = nil;
	NSString* pDevName = @"Not selected.";
	SMInDevCtrl inDevCtrl;
	
	//MIDI INの設定を取得
	[m_pUserConf setCategory:MT_CONF_CATEGORY_MIDI];
	[m_pUserConf setSection:MT_CONF_SECTION_MIDIIN];
	pSelectedDevIdName = [m_pUserConf strValueForKey:@"PortA" defaultValue:@""];
	
	//MIDI入力デバイス制御初期化
	result = inDevCtrl.Initialize();
	if (result != 0) goto EXIT;
	
	//選択入力デバイスの名称を取得
	for (i = 0; i < inDevCtrl.GetDevNum(); i++) {
		pDevIdName = inDevCtrl.GetDevIdName(i);
		if ([pDevIdName isEqualToString:pSelectedDevIdName]) {
			pDevName = inDevCtrl.GetDevDisplayName(i);
			break;
		}
	}
	
	//ラベルにデバイス名称を設定
	m_pDeviceLabel.text = pDevName;
	
EXIT:;
	if (result != 0) YN_SHOW_ERR();
	return;
}

//******************************************************************************
// ビュー非表示
//******************************************************************************
- (void)viewWillDisappear:(BOOL)animated
{
	return;
}

//******************************************************************************
// 通知送信処理
//******************************************************************************
- (int)postNotificationWithName:(NSString*)pName
{
	int result = 0;
	NSNotification* pNotification = nil;
	NSNotificationCenter* pCenter = nil;
	
	//通知オブジェクトを作成
    pNotification = [NSNotification notificationWithName:pName
												  object:self
												userInfo:nil];
	//通知する
	pCenter = [NSNotificationCenter defaultCenter];
	
	//通知に対応する処理を演奏スレッドで処理させる場合
	//[pCenter postNotification:pNotification];
	
	//通知に対応する処理をメインスレッドに処理させる場合
	[pCenter performSelectorOnMainThread:@selector(postNotification:)
							  withObject:pNotification
						   waitUntilDone:NO];
	
	return result;
}

//******************************************************************************
// モニタ開始ボタン押下
//******************************************************************************
- (IBAction)onStartMonitoringButton
{	
	//モニタ開始通知
	[self postNotificationWithName:@"onStartMonitoring"];
	
	return;
}


@end

