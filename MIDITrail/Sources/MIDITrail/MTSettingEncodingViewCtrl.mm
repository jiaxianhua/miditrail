//******************************************************************************
//
// MIDITrail / MTSettingEncodingViewCtrl
//
// エンコーディング設定ビュークラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "MTParam.h"
#import "MTSettingEncodingViewCtrl.h"


//******************************************************************************
// プライベートメソッド定義
//******************************************************************************
@interface MTSettingEncodingViewCtrl ()

@end


@implementation MTSettingEncodingViewCtrl

//******************************************************************************
// 初期化
//******************************************************************************
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	int result = 0;
	
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		//ビュー設定
		self.title = @"Character Encoding";
		
		//エンコーディングリスト初期化
		m_StringEncodingList.Initialize();
		m_EncodingId = 0;
		
		//ユーザ設定初期化
		m_pUserConf = [[YNUserConf alloc] init];
		if (m_pUserConf == nil) {
			result = YN_SET_ERR(@"Program error.", 0, 0);
			goto EXIT;
		}
    }
	
EXIT:;
	if (result != 0) YN_SHOW_ERR();
    return self;
}

//******************************************************************************
// ビュー登録完了
//******************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	return;
}

//******************************************************************************
// ビュー解除完了
//******************************************************************************
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
	
	return;
}

//******************************************************************************
// インターフェース自動回転確認
//******************************************************************************
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//******************************************************************************
// インターフェース自動回転確認（iOS6以降）
//******************************************************************************
- (BOOL)shouldAutorotate
{
	//回転を許可する
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認：回転方向（iOS6以降）
//******************************************************************************
- (NSUInteger)supportedInterfaceOrientations
{
	//全方向に対応する
	return UIInterfaceOrientationMaskAll;
}

//******************************************************************************
// ビュー表示
//******************************************************************************
- (void)viewWillAppear:(BOOL)animated
{	
	return;
}

//******************************************************************************
// ビュー非表示
//******************************************************************************
- (void)viewWillDisappear:(BOOL)animated
{
	return;
}

//******************************************************************************
// セクション数
//******************************************************************************
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    return 1;
} 

//******************************************************************************
// セクションヘッダ
//******************************************************************************
- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
} 

//******************************************************************************
// セクションごとの項目数
//******************************************************************************
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
	NSInteger numOfRows = 0;
	
	switch (section) {
		case 0:
			//ビューモード
			numOfRows = m_StringEncodingList.GetSize();
			break;
		default:
			break;
	}
	
	return numOfRows;
}

//******************************************************************************
// 項目表示内容
//******************************************************************************
- (UITableViewCell*)tableView:(UITableView*)tableView
		cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* pCellIdentifier = @"MTSettingEncodingViewCtrl";
	UITableViewCell* pCell = nil;
	NSString* pDefaultEncodingName = nil;
	NSString* pSelectedEncodingName = nil;
	NSString* pEncodingName = nil;
	
	//再利用可能セル生成
	pCell = [m_pTableView dequeueReusableCellWithIdentifier:pCellIdentifier];
	if (pCell == nil) {
		pCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
									   reuseIdentifier:pCellIdentifier];
	}
	
	//範囲外なら処理終了
	if (m_StringEncodingList.GetSize() <= indexPath.row) goto EXIT;
	
	//カテゴリ／セクション設定
	[m_pUserConf setCategory:MT_CONF_CATEGORY_PREF];
	[m_pUserConf setSection:MT_CONF_SECTION_MIDIFILE];
	
	//エンコーディング選択状態を取得
	pDefaultEncodingName = m_StringEncodingList.GetDefaultEncodingName();
	pSelectedEncodingName = [m_pUserConf strValueForKey:@"Encoding" defaultValue:pDefaultEncodingName];
	
	//ラベル設定
	pEncodingName = m_StringEncodingList.GetEncodingName(indexPath.row);
	pCell.textLabel.text = pEncodingName;
	
	//チェックマーク設定
	pCell.accessoryType = UITableViewCellAccessoryNone;
	if ([pEncodingName isEqualToString:pSelectedEncodingName]) {
		pCell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	
EXIT:;
	return pCell;
}

//******************************************************************************
// テーブルセル選択イベント
//******************************************************************************
- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* pSelectedEncodingName = nil;
	
	//選択状態解除
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	//選択行のエンコーディング名称を取得
	pSelectedEncodingName = m_StringEncodingList.GetEncodingName(indexPath.row);
	
	//選択されたエンコーディング名称を保存
	[m_pUserConf setCategory:MT_CONF_CATEGORY_PREF];
	[m_pUserConf setSection:MT_CONF_SECTION_MIDIFILE];
	[m_pUserConf setStr:pSelectedEncodingName forKey:@"Encoding"];
	
	//再表示
	[tableView performSelector:@selector(reloadData) withObject:nil afterDelay:0.1f];
	
	//1階層戻る
	//[self.navigationController popViewControllerAnimated:YES];
	
	return;
}

//******************************************************************************
// デフォルトエンコーディング名称取得
//******************************************************************************
- (NSString*)selectedEncodingName
{
	NSString* pDefaultEncodingName = nil;
	NSString* pSelectedEncodingName = nil;
	
	//カテゴリ／セクション設定
	[m_pUserConf setCategory:MT_CONF_CATEGORY_PREF];
	[m_pUserConf setSection:MT_CONF_SECTION_MIDIFILE];
	
	//エンコーディング選択状態を取得
	pDefaultEncodingName = m_StringEncodingList.GetDefaultEncodingName();
	pSelectedEncodingName = [m_pUserConf strValueForKey:@"Encoding" defaultValue:pDefaultEncodingName];
	
	return pSelectedEncodingName;	
}

//******************************************************************************
// 選択エンコーディングID取得
//******************************************************************************
- (NSStringEncoding)selectedEncodingId
{
	NSStringEncoding encodingId = 0;
	NSString* pSelectedEncodingName = nil;
	NSString* pEncodingName = nil;
	int index = 0;
	
	//エンコーディング選択状態を取得
	pSelectedEncodingName = [self selectedEncodingName];
	
	//デフォルトエンコーディングID
	encodingId = m_StringEncodingList.GetDefaultEncodingId();
	
	//選択エンコーディングIDを検索
	for (index = 0; index < m_StringEncodingList.GetSize(); index++) {
		pEncodingName = m_StringEncodingList.GetEncodingName(index);
		if ([pEncodingName isEqualToString:pSelectedEncodingName]) {
			encodingId = m_StringEncodingList.GetEncodingId(index);
			break;
		}
	}
	
	return encodingId;
}


@end

