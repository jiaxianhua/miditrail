//******************************************************************************
//
// MIDITrail / MTHelpViewCtrl
//
// ヘルプ画面制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "YNBaseLib.h"
#import "MTHelpViewCtrl.h"


//******************************************************************************
// プライベートメソッド定義
//******************************************************************************
@interface MTHelpViewCtrl ()

@end


@implementation MTHelpViewCtrl

//******************************************************************************
// 生成
//******************************************************************************
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //ビュー設定
		self.title = @"Help";
		self.tabBarItem.image = [UIImage imageNamed:@"img/TabIcon"];
    }
    return self;
}

//******************************************************************************
// ビュー登録完了
//******************************************************************************
- (void)viewDidLoad
{
	NSArray* pLanguages = nil;
	NSString* pLanguage = nil;
	NSString* pFileName = nil;
	NSString* pHtmlPath = nil;
	
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	//選択されている言語を取得
	pLanguages = [NSLocale preferredLanguages];
	pLanguage = [pLanguages objectAtIndex:0];
	
	//言語によってファイル名を切り替える
	if ([pLanguage isEqualToString:@"ja"]) {
		pFileName = @"doc/HELP.ja.html";
	}
	else {
		pFileName = @"doc/HELP.en.html";
	}
	
	//HTMLファイルパス生成
	pHtmlPath = [NSString stringWithFormat:@"%@/%@", [YNPathUtil resourceDirPath], pFileName];
	
	//HTMLファイル表示
	m_pWebView.dataDetectorTypes = UIDataDetectorTypeNone;
	m_pWebView.scalesPageToFit = YES;
	[m_pWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:pHtmlPath]]];
	
	return;
}

//******************************************************************************
// ビュー解除完了
//******************************************************************************
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
	
	return;
}

//******************************************************************************
// インターフェース自動回転確認
//******************************************************************************
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認（iOS6以降）
//******************************************************************************
- (BOOL)shouldAutorotate
{
	//回転を許可する
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認：回転方向（iOS6以降）
//******************************************************************************
- (NSUInteger)supportedInterfaceOrientations
{
	//全方向に対応する
	return UIInterfaceOrientationMaskAll;
}

//******************************************************************************
// ビュー表示
//******************************************************************************
- (void)viewWillAppear:(BOOL)animated
{
	return;
}

//******************************************************************************
// ビュー非表示
//******************************************************************************
- (void)viewWillDisappear:(BOOL)animated
{
	return;
}


@end

