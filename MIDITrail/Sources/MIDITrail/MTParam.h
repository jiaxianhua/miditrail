//******************************************************************************
//
// MIDITrail / MTParam.h
//
// パラメータ定義ファイル
//
// Copyright (C) 2010-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************


//******************************************************************************
// パラメータ定義
//******************************************************************************

//シーン種別
enum SceneType {
	Title,			//タイトル
	PianoRoll3D,	//ピアノロール3D
	PianoRoll2D,	//ピアノロール2D
	PianoRollRain,	//ピアノロールレイン
	PianoRollRain2D	//ピアノロールレイン2D
};

//演奏状態
enum PlayStatus {
	NoData,			//データなし
	Stop,			//停止状態
	Play,			//再生中
	Pause,			//一時停止
	MonitorOFF,		//モニタ停止
	MonitorON		//モニタ中
};


//******************************************************************************
// ファイルパス定義
//******************************************************************************

//ユーザ設定
#define MT_CONF_CATEGORY_PREF		@"Preference"
#define MT_CONF_SECTION_MIDIFILE		@"MIDIFile"
#define MT_CONF_CATEGORY_VIEW		@"View"
#define MT_CONF_SECTION_SCENE			@"Scene"
#define MT_CONF_SECTION_DISPLAY			@"Display"
#define MT_CONF_SECTION_VIEWPOINT		@"Viewpoint-"
#define MT_CONF_SECTION_WINDOWSIZE		@"WindowSize"
#define MT_CONF_SECTION_HOWTOVIEW		@"HowToView"
#define MT_CONF_CATEGORY_MIDI		@"MIDI"
#define MT_CONF_SECTION_MIDIOUT			@"MIDIOUT"
#define MT_CONF_SECTION_MIDIIN			@"MIDIIN"
#define MT_CONF_CATEGORY_GRAPHIC	@"Graphic"
#define MT_CONF_SECTION_AA				@"Anti-aliasing"

//アプリケーション設定ファイル：Resourcesフォルダからの相対パス
#define MT_CONFFILE_DIR				@"conf/"

//画像ファイル：Resourcesフォルダからの相対パス
#define MT_IMGFILE_RIPPLE			@"data/Ripple.png"
#define MT_IMGFILE_BOARD			@"data/Board.png"
#define MT_IMGFILE_KEYBOARD			@"data/Keyboard.png"
#define MT_IMGFILE_HOWTOVIEW1		@"data/HowToView1.png"
#define MT_IMGFILE_HOWTOVIEW2		@"data/HowToView2.png"
#define MT_IMGFILE_HOWTOVIEW3		@"data/HowToView3.png"
#define MT_IMGFILE_ICON				@"data/Icon.png"

//マニュアルファイル：Resourcesフォルダからの相対パス
#define MT_MANUALFILE				@"doc/index.html"


