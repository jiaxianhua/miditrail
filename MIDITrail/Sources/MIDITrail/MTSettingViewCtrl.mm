//******************************************************************************
//
// MIDITrail / MTSettingViewCtrl
//
// 設定画面制御クラス
//
// Copyright (C) 2012-2014 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "MTSettingViewCtrl.h"
#import "MTParam.h"
#import <QuartzCore/QuartzCore.h>


//******************************************************************************
// プライベートメソッド定義
//******************************************************************************
@interface MTSettingViewCtrl ()

//テーブルセル作成：表示項目
- (UITableViewCell*)makeDisplayItemCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル作成：MIDI OUT Device
- (UITableViewCell*)makeMIDIOUTCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル作成：MIDI IN Device
- (UITableViewCell*)makeMIDIINCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル作成：MIDI IN Monitor
- (UITableViewCell*)makeMIDIINMonitorCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル作成：MIDI File
- (UITableViewCell*)makeMIDIFileCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント：ビューモード
- (void)onSelectViewModeCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント：表示項目
- (void)onSelectDisplayItemCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント：MIDI OUT Device
- (void)onSelectMIDIOUTCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント：MIDI IN Device
- (void)onSelectMIDIINCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント：MIDI IN Monitor
- (void)onSelectMIDIINMonitorCellForIndexPath:(NSIndexPath*)indexPath;

//テーブルセル選択イベント：MIDI File
- (void)onSelectMIDIFileCellForIndexPath:(NSIndexPath*)indexPath;

@end


@implementation MTSettingViewCtrl

//******************************************************************************
// 生成
//******************************************************************************
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	NSString* pNibName = nil;
	
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	
    if (self) {
        //ビュー設定
		self.title = @"Settings";
		self.tabBarItem.image = [UIImage imageNamed:@"img/TabIcon"];
		
		//ビュー制御の生成
		if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
			//iPhone / iPod touch
			pNibName = @"MTSettingEncodingView_iPhone";
		}
		else {
			//iPad
			pNibName = @"MTSettingEncodingView_iPad";
		}
		m_pSettingEncodingViewCtrl = [[MTSettingEncodingViewCtrl alloc] initWithNibName:pNibName bundle:nil];
    }
	
    return self;
}

//******************************************************************************
// ビュー登録完了
//******************************************************************************
- (void)viewDidLoad
{
	int result = 0;
	
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	//ユーザ設定初期化
	m_pUserConf = [[YNUserConf alloc] init];
	if (m_pUserConf == nil) {
		result = YN_SET_ERR(@"Program error.", 0, 0);
		goto EXIT;
	}
	
	//ビューモードアイコン画像読み込み
	m_pImagePianoRoll3D   = [UIImage imageNamed:@"img/ViewMode-PianoRoll3D"];
	m_pImagePianoRoll2D   = [UIImage imageNamed:@"img/ViewMode-PianoRoll2D"];
	m_pImagePianoRollRain = [UIImage imageNamed:@"img/ViewMode-PianoRollRain"];
	m_pImagePianoRollRain2D = [UIImage imageNamed:@"img/ViewMode-PianoRollRain2D"];
	
EXIT:;
	if (result != 0) YN_SHOW_ERR();
}

//******************************************************************************
// ビュー解除完了
//******************************************************************************
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
	
	m_pUserConf = nil;
	
	
	return;
}

//******************************************************************************
// インターフェース自動回転確認
//******************************************************************************
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認（iOS6以降）
//******************************************************************************
- (BOOL)shouldAutorotate
{
	//回転を許可する
	return YES;
}

//******************************************************************************
// インターフェース自動回転確認：回転方向（iOS6以降）
//******************************************************************************
- (NSUInteger)supportedInterfaceOrientations
{
	//全方向に対応する
	return UIInterfaceOrientationMaskAll;
}

//******************************************************************************
// ビュー表示
//******************************************************************************
- (void)viewWillAppear:(BOOL)animated
{
	int result = 0;
	
	//MIDI出力デバイス制御初期化
	result = m_OutDevCtrl.Initialize();
	if (result != 0) goto EXIT;
	
	//MIDI入力デバイス制御初期化
	result = m_InDevCtrl.Initialize();
	if (result != 0) goto EXIT;
	
	//テーブルビュー再表示
	[m_pTableView reloadData];
	
EXIT:;
	if (result != 0) YN_SHOW_ERR();
	return;
}

//******************************************************************************
// ビュー非表示
//******************************************************************************
- (void)viewWillDisappear:(BOOL)animated
{
	return;
}

//******************************************************************************
// セクション数
//******************************************************************************
-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
	//セクション
	// 0. View Mode
	// 1. Display Item
	// 2. MIDI OUT Device
	// 3. MIDI IN Device
	// 4. MIDI IN Monitor
	// 5. MIDI File
	
    return 6;
} 

//******************************************************************************
// セクションヘッダ
//******************************************************************************
- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString* pSectionHeader = nil;
	
	switch(section) {
		case 0:
			pSectionHeader = @"View Mode";
			break;
		case 1:
			pSectionHeader = @"Display Item";
			break;
		case 2:
			pSectionHeader = @"MIDI OUT Device";
			break;
		case 3:
			pSectionHeader = @"MIDI IN Device";
			break;
		case 4:
			pSectionHeader = @"MIDI IN Monitor";
			break;
		case 5:
			pSectionHeader = @"MIDI File";
			break;
		default:
			break;
    }
	
    return pSectionHeader;
} 

//******************************************************************************
// セクションごとの項目数
//******************************************************************************
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
	NSInteger numOfRows = 0;
	unsigned long devNum = 0;
	
	//NSLog(@"section %d", section);
	
	switch (section) {
		case 0:
			//View Mode
			numOfRows = 4;
			break;
		case 1:
			//Display Item
			numOfRows = 5;
			break;
		case 2:
			//MIDI OUT Device
			devNum = m_OutDevCtrl.GetDevNum();
			if (devNum == 0) {
				numOfRows = 1;
			}
			else {
				numOfRows = devNum;
			}
			break;
		case 3:
			//MIDI IN Device
			devNum = m_InDevCtrl.GetDevNum();
			if (devNum == 0) {
				numOfRows = 1;
			}
			else {
				numOfRows = devNum;
			}
			break;
		case 4:
			//MIDI Monitor
			numOfRows = 1;
			break;
		case 5:
			//MIDI File
			numOfRows = 1;
			break;
		default:
			break;
	}
	
	return numOfRows;
}

//******************************************************************************
// 項目表示内容
//******************************************************************************
- (UITableViewCell*)tableView:(UITableView*)tableView
		 cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	UITableViewCell* pCell = nil;
	
	switch (indexPath.section) {
		case 0:
			//ビューモード
			pCell = [self makeViewModeCellForIndexPath:indexPath];
			break;
		case 1:
			//表示項目
			pCell = [self makeDisplayItemCellForIndexPath:indexPath];
			break;
		case 2:
			//MIDI OUT Device
			pCell = [self makeMIDIOUTCellForIndexPath:indexPath];
			break;
		case 3:
			//MIDI IN Device
			pCell = [self makeMIDIINCellForIndexPath:indexPath];
			break;
		case 4:
			//MIDI IN Monitor
			pCell = [self makeMIDIINMonitorCellForIndexPath:indexPath];
			break;
		case 5:
			//MIDI File
			pCell = [self makeMIDIFileCellForIndexPath:indexPath];
			break;
		default:
			break;
	}
	
    return pCell;
}

//******************************************************************************
// テーブルセル作成：ビューモード
//******************************************************************************
- (UITableViewCell*)makeViewModeCellForIndexPath:(NSIndexPath*)indexPath
{
    static NSString* pCellIdentifier = @"MTSettingViewCtrl_ViewMode";
	UITableViewCell* pCell = nil;
	NSString* pSelectedType = nil;
	NSString* pType = nil;
	
	//再利用可能セル生成
	pCell = [m_pTableView dequeueReusableCellWithIdentifier:pCellIdentifier];
	if (pCell == nil) {
		pCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
									   reuseIdentifier:pCellIdentifier];
	}
	
	//ラベル設定
	switch (indexPath.row) {
		case 0:
			pCell.textLabel.text = @"Piano Roll 3D";
			pCell.imageView.image = m_pImagePianoRoll3D;
			pCell.imageView.layer.cornerRadius = 8.0;
			pCell.imageView.layer.masksToBounds = YES;
			pType = @"PianoRoll3D";
			break;			
		case 1:
			pCell.textLabel.text = @"Piano Roll 2D";
			pCell.imageView.image = m_pImagePianoRoll2D;
			pCell.imageView.layer.cornerRadius = 8.0;
			pCell.imageView.layer.masksToBounds = YES;
			pType = @"PianoRoll2D";
			break;			
		case 2:
			pCell.textLabel.text = @"Piano Roll Rain";
			pCell.imageView.image = m_pImagePianoRollRain;
			pCell.imageView.layer.cornerRadius = 8.0;
			pCell.imageView.layer.masksToBounds = YES;
			pType = @"PianoRollRain";
			break;
		case 3:
			pCell.textLabel.text = @"Piano Roll Rain 2D";
			pCell.imageView.image = m_pImagePianoRollRain2D;
			pCell.imageView.layer.cornerRadius = 8.0;
			pCell.imageView.layer.masksToBounds = YES;
			pType = @"PianoRollRain2D";
			break;
		default:
			break;
	}
	
	//選択中シーン種別を取得
	[m_pUserConf setCategory:MT_CONF_CATEGORY_VIEW];
	[m_pUserConf setSection:MT_CONF_SECTION_SCENE];
	pSelectedType = [m_pUserConf strValueForKey:@"Type" defaultValue:@"PianoRoll3D"];
	
	//チェックマーク設定
	pCell.accessoryType = UITableViewCellAccessoryNone;
	if ([pType isEqualToString:pSelectedType]) {
		pCell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	
	return pCell;
}

//******************************************************************************
// テーブルセル作成：表示項目
//******************************************************************************
- (UITableViewCell*)makeDisplayItemCellForIndexPath:(NSIndexPath*)indexPath
{
    static NSString* pCellIdentifier = @"MTSettingViewCtrl_DisplayItem";
	UITableViewCell* pCell = nil;
	NSString* pItemName = nil;
	int value = 0;
	
	//再利用可能セル生成
	pCell = [m_pTableView dequeueReusableCellWithIdentifier:pCellIdentifier];
	if (pCell == nil) {
		pCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
									   reuseIdentifier:pCellIdentifier];
	}
	
	switch (indexPath.row) {
		case 0:
			pCell.textLabel.text = @"Piano Keyboard";
			pItemName = @"PianoKeyboard";
			break;			
		case 1:
			pCell.textLabel.text = @"Ripple";
			pItemName = @"Ripple";
			break;			
		case 2:
			pCell.textLabel.text = @"Pitch Bend Motion";
			pItemName = @"PitchBendMotion";
			break;			
		case 3:
			pCell.textLabel.text = @"Stars";
			pItemName = @"Stars";
			break;			
		case 4:
			pCell.textLabel.text = @"Counter";
			pItemName = @"Counter";
			break;
		default:
			break;
	}
	
	//表示項目の表示設定を取得
	[m_pUserConf setCategory:MT_CONF_CATEGORY_VIEW];
	[m_pUserConf setSection:MT_CONF_SECTION_DISPLAY];
	value = [m_pUserConf intValueForKey:pItemName defaultValue:1];
	
	//チェックマーク設定
	pCell.accessoryType = UITableViewCellAccessoryNone;
	if (value == 1) {
		pCell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	
	return pCell;
}

//******************************************************************************
// テーブルセル作成：MIDI OUT Device
//******************************************************************************
- (UITableViewCell*)makeMIDIOUTCellForIndexPath:(NSIndexPath*)indexPath
{
    static NSString* pCellIdentifier = @"MTSettingViewCtrl_MIDIOUT";
	UITableViewCell* pCell = nil;
	NSString* pDevIdName = nil;
	NSString* pSelectedDevIdName = nil;
	
	//再利用可能セル生成
	pCell = [m_pTableView dequeueReusableCellWithIdentifier:pCellIdentifier];
	if (pCell == nil) {
		pCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
									   reuseIdentifier:pCellIdentifier];
	}
	
	//テキスト設定
	if (m_OutDevCtrl.GetDevNum() == 0) {
		pCell.textLabel.text = @"not found";
		pDevIdName = @"";
	}
	else {
		pCell.textLabel.text = m_OutDevCtrl.GetDevDisplayName(indexPath.row);
		pDevIdName = m_OutDevCtrl.GetDevIdName(indexPath.row);
		pCell.detailTextLabel.text = m_OutDevCtrl.GetManufacturerName(indexPath.row);
	}
	
	//MIDI OUTの設定を取得
	[m_pUserConf setCategory:MT_CONF_CATEGORY_MIDI];
	[m_pUserConf setSection:MT_CONF_SECTION_MIDIOUT];
	pSelectedDevIdName = [m_pUserConf strValueForKey:@"PortA" defaultValue:@""];
	
	//チェックマーク設定
	pCell.accessoryType = UITableViewCellAccessoryNone;
	if ([pSelectedDevIdName length] > 0) {
		if ([pSelectedDevIdName isEqualToString:pDevIdName]) {
			pCell.accessoryType = UITableViewCellAccessoryCheckmark;
		}
	}
	
	return pCell;
}

//******************************************************************************
// テーブルセル作成：MIDI IN Device
//******************************************************************************
- (UITableViewCell*)makeMIDIINCellForIndexPath:(NSIndexPath*)indexPath
{
    static NSString* pCellIdentifier = @"MTSettingViewCtrl_MIDIIN";
	UITableViewCell* pCell = nil;
	NSString* pDevIdName = nil;
	NSString* pSelectedDevIdName = nil;
	
	//再利用可能セル生成
	pCell = [m_pTableView dequeueReusableCellWithIdentifier:pCellIdentifier];
	if (pCell == nil) {
		pCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
									   reuseIdentifier:pCellIdentifier];
	}
	
	//テキスト設定
	if (m_InDevCtrl.GetDevNum() == 0) {
		pCell.textLabel.text = @"not found";
		pDevIdName = @"";
	}
	else {
		pCell.textLabel.text = m_InDevCtrl.GetDevDisplayName(indexPath.row);
		pDevIdName = m_InDevCtrl.GetDevIdName(indexPath.row);
		pCell.detailTextLabel.text = m_InDevCtrl.GetManufacturerName(indexPath.row);
	}
	
	//MIDI INの設定を取得
	[m_pUserConf setCategory:MT_CONF_CATEGORY_MIDI];
	[m_pUserConf setSection:MT_CONF_SECTION_MIDIIN];
	pSelectedDevIdName = [m_pUserConf strValueForKey:@"PortA" defaultValue:@""];
	
	//チェックマーク設定
	pCell.accessoryType = UITableViewCellAccessoryNone;
	if ([pSelectedDevIdName length] > 0) {
		if ([pSelectedDevIdName isEqualToString:pDevIdName]) {
			pCell.accessoryType = UITableViewCellAccessoryCheckmark;
		}
	}
	
	return pCell;
}

//******************************************************************************
// テーブルセル作成：MIDI IN Monitor
//******************************************************************************
- (UITableViewCell*)makeMIDIINMonitorCellForIndexPath:(NSIndexPath*)indexPath
{
    static NSString* pCellIdentifier = @"MTSettingViewCtrl_MIDIINMonitor";
	UITableViewCell* pCell = nil;
	int checkMIDITHRU = 0;
	
	//再利用可能セル生成
	pCell = [m_pTableView dequeueReusableCellWithIdentifier:pCellIdentifier];
	if (pCell == nil) {
		pCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
									   reuseIdentifier:pCellIdentifier];
	}
	
	//テキスト設定
	pCell.textLabel.text = @"MIDI THRU";
	
	//MIDI INの設定を取得
	[m_pUserConf setCategory:MT_CONF_CATEGORY_MIDI];
	[m_pUserConf setSection:MT_CONF_SECTION_MIDIIN];
	checkMIDITHRU = [m_pUserConf intValueForKey:@"MIDITHRU" defaultValue:1];
	
	//チェックマーク設定
	pCell.accessoryType = UITableViewCellAccessoryNone;
	if (checkMIDITHRU > 0) {
		pCell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	
	return pCell;
}

//******************************************************************************
// テーブルセル作成：MIDI File
//******************************************************************************
- (UITableViewCell*)makeMIDIFileCellForIndexPath:(NSIndexPath*)indexPath
{
    static NSString* pCellIdentifier = @"MTSettingViewCtrl_MIDIFile";
	UITableViewCell* pCell = nil;
	NSString* pEncodingName = nil;
	
	//再利用可能セル生成
	pCell = [m_pTableView dequeueReusableCellWithIdentifier:pCellIdentifier];
	if (pCell == nil) {
		pCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
									   reuseIdentifier:pCellIdentifier];
	}
		
	//エンコーディング選択状態を取得
	pEncodingName = [m_pSettingEncodingViewCtrl selectedEncodingName];
	
	//テキスト設定
	pCell.textLabel.text = @"Character Encoding";
	pCell.detailTextLabel.text = pEncodingName;
	
	//階層マーク設定
	pCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	return pCell;
}

//******************************************************************************
// テーブルセル作成：列の高さ
//******************************************************************************
- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
	CGFloat height = 0.0f;
	
	switch (indexPath.section) {
		case 0:
			//ビューモード
			height = 86.0f;
			break;
		default:
			//デフォルトの高さを返す
			height = m_pTableView.rowHeight;
			break;
	}
	
	return height;
}

//******************************************************************************
// テーブルセル選択イベント
//******************************************************************************
- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
	//選択状態解除
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	switch (indexPath.section) {
		case 0:
			//View Mode
			[self onSelectViewModeCellForIndexPath:indexPath];
			break;
		case 1:
			//Display Item
			[self onSelectDisplayItemCellForIndexPath:indexPath];
			break;
		case 2:
			//MIDI OUT Device
			[self onSelectMIDIOUTCellForIndexPath:indexPath];
			break;
		case 3:
			//MIDI IN Device
			[self onSelectMIDIINCellForIndexPath:indexPath];
			break;
		case 4:
			//MIDI IN Monitor
			[self onSelectMIDIINMonitorCellForIndexPath:indexPath];
			break;
		case 5:
			//MIDI File
			[self onSelectMIDIFileCellForIndexPath:indexPath];
			break;
		default:
			break;
	}
	
	[tableView performSelector:@selector(reloadData) withObject:nil afterDelay:0.1f];
	
	return;
}

//******************************************************************************
// テーブルセル選択イベント：ビューモード
//******************************************************************************
- (void)onSelectViewModeCellForIndexPath:(NSIndexPath*)indexPath
{
	NSString* pSelectedType = nil;
	
	//NSLog(@"selected %d", indexPath.row);
	
	switch (indexPath.row) {
		case 0:
			pSelectedType = @"PianoRoll3D";
			break;			
		case 1:
			pSelectedType = @"PianoRoll2D";
			break;			
		case 2:
			pSelectedType = @"PianoRollRain";
			break;
		case 3:
			pSelectedType = @"PianoRollRain2D";
			break;
		default:
			break;
	}
	
	//選択中シーン種別を保存
	[m_pUserConf setCategory:MT_CONF_CATEGORY_VIEW];
	[m_pUserConf setSection:MT_CONF_SECTION_SCENE];
	[m_pUserConf setStr:pSelectedType forKey:@"Type"];
	
	return;
}

//******************************************************************************
// テーブルセル選択イベント：表示項目
//******************************************************************************
- (void)onSelectDisplayItemCellForIndexPath:(NSIndexPath*)indexPath
{
	NSString* pItemName = nil;
	int value = 0;
	
	//NSLog(@"selected %d", indexPath.row);
	
	switch (indexPath.row) {
		case 0:
			pItemName = @"PianoKeyboard";
			break;			
		case 1:
			pItemName = @"Ripple";
			break;			
		case 2:
			pItemName = @"PitchBendMotion";
			break;			
		case 3:
			pItemName = @"Stars";
			break;			
		case 4:
			pItemName = @"Counter";
			break;
		default:
			break;
	}
	
	//表示項目の表示設定を取得
	[m_pUserConf setCategory:MT_CONF_CATEGORY_VIEW];
	[m_pUserConf setSection:MT_CONF_SECTION_DISPLAY];
	value = [m_pUserConf intValueForKey:pItemName defaultValue:1];
	
	//選択状態を反転して保存
	value = (value == 1)? 0 : 1;
	[m_pUserConf setInt:value forKey:pItemName];
	
	return;
}

//******************************************************************************
// テーブルセル選択イベント：MIDI OUT Device
//******************************************************************************
- (void)onSelectMIDIOUTCellForIndexPath:(NSIndexPath*)indexPath
{
	NSString* pSelectedDevIdName = nil;
	
	//NSLog(@"selected %d", indexPath.row);
	
	if (m_OutDevCtrl.GetDevNum() != 0) {
		pSelectedDevIdName = m_OutDevCtrl.GetDevIdName(indexPath.row);
		
		//MIDI OUT設定を保存
		[m_pUserConf setCategory:MT_CONF_CATEGORY_MIDI];
		[m_pUserConf setSection:MT_CONF_SECTION_MIDIOUT];
		[m_pUserConf setStr:pSelectedDevIdName forKey:@"PortA"];
	}
		
	return;
}

//******************************************************************************
// テーブルセル選択イベント：MIDI IN Device
//******************************************************************************
- (void)onSelectMIDIINCellForIndexPath:(NSIndexPath*)indexPath
{
	NSString* pSelectedDevIdName = nil;
	
	//NSLog(@"selected %d", indexPath.row);
	
	if (m_InDevCtrl.GetDevNum() != 0) {
		pSelectedDevIdName = m_InDevCtrl.GetDevIdName(indexPath.row);
		
		//MIDI IN設定を保存
		[m_pUserConf setCategory:MT_CONF_CATEGORY_MIDI];
		[m_pUserConf setSection:MT_CONF_SECTION_MIDIIN];
		[m_pUserConf setStr:pSelectedDevIdName forKey:@"PortA"];
	}
	
	return;
}

//******************************************************************************
// テーブルセル選択イベント：MIDI IN Monitor
//******************************************************************************
- (void)onSelectMIDIINMonitorCellForIndexPath:(NSIndexPath*)indexPath
{
	int checkMIDITHRU = 0;
	
	//NSLog(@"selected %d", indexPath.row);
	
	//MIDI INの設定を取得
	[m_pUserConf setCategory:MT_CONF_CATEGORY_MIDI];
	[m_pUserConf setSection:MT_CONF_SECTION_MIDIIN];
	checkMIDITHRU = [m_pUserConf intValueForKey:@"MIDITHRU" defaultValue:1];
	
	//選択状態を反転
	if (checkMIDITHRU == 0) {
		checkMIDITHRU = 1;
	}
	else {
		checkMIDITHRU = 0;
	}
	
	[m_pUserConf setInt:checkMIDITHRU forKey:@"MIDITHRU"];
	
	return;
}

//******************************************************************************
// テーブルセル選択イベント：MIDI File
//******************************************************************************
- (void)onSelectMIDIFileCellForIndexPath:(NSIndexPath*)indexPath
{
	//NSLog(@"selected %d", indexPath.row);
	
	if (indexPath.row == 0) {
		//エンコーディング選択ビュー表示
		[self.navigationController pushViewController:m_pSettingEncodingViewCtrl animated:YES];
	}
	
	return;
}

//******************************************************************************
// 選択エンコーディングID取得
//******************************************************************************
- (NSStringEncoding)selectedEncodingId
{
	return [m_pSettingEncodingViewCtrl selectedEncodingId];
}


@end

