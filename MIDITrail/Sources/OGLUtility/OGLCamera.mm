//******************************************************************************
//
// OpenGL Utility / OGLCamera
//
// カメラクラス
//
// Copyright (C) 2010-2012 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "OGLCamera.h"
#import "OGLH.h"
#import "YNBaseLib.h"


//******************************************************************************
// コンストラクタ
//******************************************************************************
OGLCamera::OGLCamera(void)
{
	_Clear();
}

//******************************************************************************
// デストラクタ
//******************************************************************************
OGLCamera::~OGLCamera(void)
{
}

//******************************************************************************
// 初期化
//******************************************************************************
int OGLCamera::Initialize()
{
	_Clear();
	return 0;
}

//******************************************************************************
// 基本パラメータ設定
//******************************************************************************
void OGLCamera::SetBaseParam(
		float viewAngle,
		float nearPlane,
		float farPlane
	)
{
	m_ViewAngle = viewAngle;
	m_NearPlane = nearPlane;
	m_FarPlane = farPlane;
}

//******************************************************************************
// カメラ位置設定
//******************************************************************************
void OGLCamera::SetPosition(
		OGLVECTOR3 camVector,
		OGLVECTOR3 camLookAtVector,
		OGLVECTOR3 camUpVector
	)
{
	m_CamVector = camVector;
	m_CamLookAtVector = camLookAtVector;
	m_CamUpVector = camUpVector;
}

//******************************************************************************
// 変換
//******************************************************************************
int OGLCamera::Transform(
		OGLDevice* pOGLDevice
	)
{
	int result = 0;
	GLenum glresult = 0;
	OGLVIEWPORT viewPort;
	float aspect = 0.0f;
	
	//ビューポート取得
	pOGLDevice->GetViewPort(&viewPort);
	
	//アスペクト比
	aspect = viewPort.width / viewPort.height;
	
	//ビューポート設定
	glViewport(
			viewPort.originx,	//左下隅の座標x
			viewPort.originy,	//左下隅の座標y
			viewPort.width * [UIScreen mainScreen].scale,	//ビューポートの幅 retina対応
			viewPort.height * [UIScreen mainScreen].scale	//ビューポートの高さ retina対応
		);
	if ((glresult = glGetError()) != GL_NO_ERROR) {
		result = YN_SET_ERR(@"OpenGL API error.", glresult, 0);
		goto EXIT;
	}
	
	//射影行列の設定
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	_gluPerspective(
			m_ViewAngle,	//視覚度
			aspect,			//アスペクト比
			m_NearPlane,	//nearプレーン
			m_FarPlane		//farプレーン
		);
	if ((glresult = glGetError()) != GL_NO_ERROR) {
		result = YN_SET_ERR(@"OpenGL API error.", glresult, 0);
		goto EXIT;
	}
	
	//視線方向設定
	_gluLookAt(
			m_CamVector.x,			//カメラ位置
			m_CamVector.y,			//
			m_CamVector.z,			//
			m_CamLookAtVector.x,	//注目点
			m_CamLookAtVector.y,	//
			m_CamLookAtVector.z,	//
			m_CamUpVector.x,		//カメラの上方向
			m_CamUpVector.y,		//
			m_CamUpVector.z			//
		);
	
EXIT:;
	return result;
}

//******************************************************************************
// クリア
//******************************************************************************
void OGLCamera::_Clear()
{
	m_ViewAngle = 45.0f;
	m_NearPlane = 1.0f;
	m_FarPlane = 1000.0f;
	m_CamVector = OGLVECTOR3(0.0f, 0.0f, 0.0f);
	m_CamLookAtVector = OGLVECTOR3(0.0f, 0.0f, 1.0f);
	m_CamUpVector = OGLVECTOR3(0.0f, 1.0f, 0.0f);
}

//******************************************************************************
// gluPerspective 代替関数
//******************************************************************************
void OGLCamera::_gluPerspective(
		GLfloat fovy,		//視覚度
		GLfloat aspect,		//アスペクト比
		GLfloat znear,		//nearプレーン
		GLfloat zfar		//farプレーン
	)
{
	GLfloat f = 0.0;
	GLfloat matrix[4][4] = {
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
	};
	
	f = 1.0 / tan(OGLH::ToRadian(fovy) / 2.0);
	
	matrix[0][0] = f / aspect;
	matrix[1][1] = f;
	matrix[2][2] = (zfar + znear) / (znear - zfar);
	matrix[2][3] = -1.0;
	matrix[3][2] = (2.0 * zfar *znear) / (znear - zfar);
	
	glMultMatrixf(&(matrix[0][0]));
	
	return;
}

//******************************************************************************
// gluLookAt 代替関数
//******************************************************************************
void OGLCamera::_gluLookAt(
		GLfloat eyeX,		//カメラ位置
		GLfloat eyeY,		//
		GLfloat eyeZ,		//
		GLfloat centerX,	//注目点
		GLfloat centerY,	//
		GLfloat centerZ,	//
		GLfloat upX,		//カメラの上方向
		GLfloat upY,		//
		GLfloat upZ			//
	)
{
	OGLVECTOR3 eyev;
	OGLVECTOR3 centerv;
	OGLVECTOR3 upv;
	OGLVECTOR3 f;
	OGLVECTOR3 s;
	OGLVECTOR3 u;
	
	eyev    = OGLVECTOR3(eyeX, eyeY, eyeZ);
	centerv = OGLVECTOR3(centerX, centerY, centerZ);
	upv     = OGLVECTOR3(upX, upY, upZ);
	
	f = centerv - eyev;
	OGLH::Vec3Normalize(&f, &f);	// f = f / |f|
	OGLH::Vec3Cross(&s, &f, &upv);	// s = f x up
	OGLH::Vec3Normalize(&s, &s);	// s = s / |s|
	OGLH::Vec3Cross(&u, &s, &f);	// u = s x f
	
	GLfloat matrix[] = {
		s.x, u.x, -(f.x), 0,
		s.y, u.y, -(f.y), 0,
		s.z, u.z, -(f.z), 0,
		0,   0,      0, 1
	};
	
	glMultMatrixf(&(matrix[0]));
	glTranslatef(-eyeX, -eyeY, -eyeZ);
	
	return;
}

