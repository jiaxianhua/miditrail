//******************************************************************************
//
// OpenGL Utility / OGLH
//
// ヘルパ関数クラス
//
// Copyright (C) 2010-2012 WADA Masashi. All Rights Reserved.
//
//******************************************************************************

#import "OGLTypes.h"


//******************************************************************************
// ヘルパ関数クラス
//******************************************************************************
class OGLH
{
public:
	
	//ベクトル正規化
	static void Vec3Normalize(
		OGLVECTOR3* pNormalizedVector,
		const OGLVECTOR3* pVector
	);
	
	//ベクトル外積
	static void Vec3Cross(
		OGLVECTOR3* pOutVector,
		const OGLVECTOR3* pInVector1,
		const OGLVECTOR3* pInVector2
	);
	
	//ラジアン算出
	static float ToRadian(float degree);

};


